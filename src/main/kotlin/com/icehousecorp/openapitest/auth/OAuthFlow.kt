package com.icehousecorp.openapitest.auth

enum class OAuthFlow {
    accessCode, implicit, password, application
}
